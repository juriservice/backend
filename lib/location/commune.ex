defmodule Location.Commune do
  @derive [Poison.Encoder]
  defstruct [:code, :name, :zip_code]
end

