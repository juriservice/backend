defmodule Location.Server do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def stop() do
    GenServer.stop(__MODULE__)
  end

  def init([]) do
    trie =
      :location
      |> :ets.match({:_, :"$1"})
      |> List.flatten
      |> Enum.reduce(:btrie.new, fn(c, t) ->
        t
        |> insert(c.name, c.code)
        |> insert(c.zip_code, c.code)
      end)
    {:ok, trie}
  end

  defp insert(trie, key, value) do
    :btrie.append(key, value, trie)
  end

  defp match(trie, key) do
    :btrie.foldl_similar(key, fn _, v, acc -> [v|acc] end, [], trie)
    |> List.flatten
  end

  def search(name) when is_binary(name) do
    GenServer.call(__MODULE__, {:search, name})
  end

  defp get_commune(code) do
    :location
    |> :ets.lookup(code)
    |> Enum.map(fn({_, c}) -> c end)
  end

  defp merge_results(results) do
    results
    |> Enum.map(fn r ->
      MapSet.new(r)
    end)
    |> Enum.reduce(results |> hd |> MapSet.new, fn r, a ->
      MapSet.intersection(a, r)
    end)
    |> MapSet.to_list
    |> Enum.map(&get_commune/1)
    |> List.flatten
  end

  defp get_results(terms, trie) do
    terms
    |> Enum.map(fn term ->
      trie |> match(term)
    end)
  end

  defp add_str(nil, s2), do: s2
  defp add_str(s1, s2), do: s1 <> " " <> s2

  defp sanitize_terms(terms) do
    terms
    |> String.upcase
    |> String.split
    # try to make ["TOWN", "zip_code"]
    |> Enum.reduce([nil, nil], fn c, [s, i] ->
      case Integer.parse(c) do
        {_, ""} ->
          [s, c]
        _ ->
          [add_str(s, c), i]
      end
    end)
    |> Enum.reject(fn term -> term == nil end)
  end

  defp search(trie, terms) do
    terms
    |> sanitize_terms
    |> get_results(trie)
    |> merge_results
  end

  def handle_call({:search, term}, _, trie) when is_binary(term) do
    {:reply, {:ok, trie |> search(term)}, trie}
  end

end
