defmodule Location.Cache do
  use GenServer

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def stop() do
    GenServer.stop(__MODULE__)
  end

  def init([]) do
    :ets.new(:location, [:bag, :named_table,
                         :protected,
                         read_concurrency: true])
    file_name = Application.get_env(:juriservice, :locations)
    file_path = Path.join(:code.priv_dir(:juriservice), file_name)
    populate(file_path)
    {:ok, []}
  end

  defp populate(file_path) do
    file_path
    |> File.stream!([], :line)
    |> Stream.map(&parse_line/1)
    |> Stream.each(&insert/1)
    |> Stream.run
  end

  defp parse_line(line) do
    [code, name, zip_code | _] = line |> String.split(";")
    %Location.Commune{code: code, name: name, zip_code: zip_code}
  end

  defp insert(c = %Location.Commune{code: code}) do
    :ets.insert(:location, {code, c})
  end

end
