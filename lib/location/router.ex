defmodule Location.Router do
  require Logger
  use Api.Helpers
  use Plug.Router

  plug Plug.Parsers, parsers: [:urlencoded]
  plug :match
  plug :json_header_plug
  plug PlugCors
  plug :dispatch

  defp response({:error, err}, conn) do
    send_resp(conn, 500, %Api.Error{error: err} |> Poison.encode!)
  end

  defp response({:ok, communes}, conn) do
    send_resp(conn, 200, %Api.Response{data: communes} |> Poison.encode!)
  end

  get "/search" do
    case conn.params |> Map.get("query") do
      nil ->
        response({:error, "No query provided"}, conn)
      query ->
        query |> Location.Server.search |> response(conn)
    end
  end

  match _ do
    send_resp(conn, 404, @not_found)
  end

end
