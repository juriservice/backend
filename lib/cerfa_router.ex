defmodule CerfaRouter do
  use Api.Helpers
  use Plug.Router

  @cerfa_url "https://www.formulaires.modernisation.gouv.fr/gf/"
  @notice_url "https://www.formulaires.modernisation.gouv.fr/gf/getNotice.do"

  plug :match
  plug :dispatch

  get "/:id" do
    case cerfa_get(id) do
      {:ok, data} ->
        send_data(conn, data)
      {:error, _} ->
        response(conn, 404, "Not found")
    end
  end

  get "/:id/notice/:notice_id" do
    case notice_get(id, notice_id) do
      {:ok, data} ->
        send_data(conn, data)
      {:error, _} ->
        response(conn, 404, "Not found")
    end
  end

  match _ do
    response(conn, 404, "Not found")
  end

  defp send_data(conn, data) do
    conn
    |> put_resp_content_type("application/pdf")
    |> send_resp(200, data)
  end

  defp cerfa_path(id) do
    base_dir = Application.app_dir(:juriservice, "priv/cerfa")
    Path.join([base_dir, id])
  end

  defp notice_path(id, notice_id) do
    base_dir = Application.app_dir(:juriservice, "priv/cerfa")
    Path.join([base_dir, id <> "_notice_" <> notice_id])
  end

  defp write(path, data) do
    File.write!(path, data)
  end

  def cerfa_url(id) do
    "#{@cerfa_url}cerfa_#{id}.do"
  end

  def notice_url(id, notice_id) do
    "#{@notice_url}?cerfaNotice=#{notice_id}&cerfaFormulaire=#{id}"
  end

  def cerfa_download(id) do
    case id |> cerfa_url |> HTTPoison.get(%{}, hackney: [:insecure]) do
      {:ok, r = %HTTPoison.Response{body: "%PDF" <> _}} ->
        id
        |> cerfa_path
        |> write(r.body)
        cerfa_get(id)
      _ ->
        {:error, :enoent}
    end
  end

  def notice_download(id, notice_id) do
    case notice_url(id, notice_id) |> HTTPoison.get(%{}, hackney: [:insecure]) do
      {:ok, r = %HTTPoison.Response{body: "%PDF" <> _}} ->
        id
        |> notice_path(notice_id)
        |> write(r.body)
        notice_get(id, notice_id)
      _ ->
        {:error, :enoent}
    end
  end

  defp cerfa_get(id) do
    path = id |> cerfa_path
    case path |> File.exists? do
      true -> {:ok, path |> File.read!}
      false -> cerfa_download(id)
    end
  end

  defp notice_get(id, notice_id) do
    path = notice_path(id, notice_id)
    case path |> File.exists? do
      true -> {:ok, path |> File.read!}
      false -> notice_download(id, notice_id)
    end
  end

end
