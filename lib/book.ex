defmodule Book do

  defmodule Info do
    @derive [Poison.Encoder]
    defstruct [type: :book,
               name: nil,
               address: [],
               url: nil,
               email: nil,
               phone: nil,
               fax: nil,
               geo: nil,
               opening_hours: []]
  end

  defmodule InfoSmall do
    @derive {Poison.Encoder, only: [:type, :name, :address, :phone, :email, :fax]}
    defstruct [type: :book_small,
               name: nil,
               address: [],
               url: nil,
               email: nil,
               phone: nil,
               fax: nil,
               geo: nil,
               opening_hours: []]
  end

end
