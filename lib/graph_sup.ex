defmodule GraphSup do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, [], [{:name, __MODULE__}])
  end

  def init([]) do
    children = [
      supervisor(Task.Supervisor, [[name: GraphEvents.Tasks]], id: :events),
      supervisor(Task.Supervisor, [[name: GraphLinkChecker.Tasks]], id: :links),
      worker(GraphEvents, [], []),
      worker(GraphDB, [], []),
      worker(Graph, [], []),
      worker(GraphLinkChecker, [], []),
    ]
    supervise(children, strategy: :one_for_one)
  end
end
