defmodule Api do

  defmodule Response do
    @derive [Poison.Encoder]
    defstruct [data: nil, error: nil, status: 200]
  end

  defmodule Error do
    @derive [Poison.Encoder]
    defstruct [data: nil, error: nil, status: 500]
  end

  defmodule Endpoint do
    @moduledoc """
    Represent an API endoint.

    base_url + path ? params
    """
    @derive {Poison.Encoder, except: [:transform]}
    defstruct [type: :api, base_url: "", path: [], params: [], transform: nil]
  end

  defmodule Helpers do

    defmacro __using__(_) do
      quote do
        import unquote(__MODULE__)

        def json_header_plug(conn, opts) do
          conn |> Plug.Conn.put_resp_content_type("application/json")
        end

        def response(conn, code, data) do
          data = %Api.Response{status: code, data: data} |> Poison.encode!
          Plug.Conn.send_resp(conn, code, data)
        end

        @bad_request %Response{status: 400, error: "All your base are belong to us"} |> Poison.encode!
        @not_found %Response{status: 404, error: "It's been nice seeing you"} |> Poison.encode!
      end
    end

  end

end
