defmodule Graph do
  require Logger

  @type digraph_type :: {:digraph, integer, integer, integer, boolean()}

  defmodule G do
    @derive [Poison.Encoder]
    defstruct nodes: [], edges: []
  end

  defmodule NodeValue do
    @derive [Poison.Encoder]
    defstruct [:default, :type, :name]
  end

  defmodule NodeValues do
    @derive [Poison.Encoder]
    defstruct [values: []]
  end

  defmodule N do
    @derive [Poison.Encoder]
    defstruct [:id, :title, :type, infos: [], order: 0, expect: %NodeValues{}]
  end

  defmodule E do
    @derive [Poison.Encoder]
    defstruct [:id, :source, :target, context: %{}]
  end

  defmodule Input do
    @derive [Poison.Encoder]
    defstruct [type: :input, tags: [], label: nil, name: nil, api: nil, help: nil]
  end

  defmodule Radio do
    @derive [Poison.Encoder]
    defstruct [type: :radio, tags: [], label: nil, name: nil, choices: [], help: nil]
  end

  defmodule RadioChoice do
    @derive [Poison.Encoder]
    defstruct [key: nil, name: nil, desc: nil]
  end

  defmodule InfoText do
    @derive [Poison.Encoder]
    defstruct [type: :text, tags: [], text: nil]
  end

  defmodule InfoNote do
    @derive [Poison.Encoder]
    defstruct [type: :note, text: nil]
  end

  defmodule InfoList do
    @derive [Poison.Encoder]
    defstruct [type: :list, tags: [], items: [], numbered: false]
  end

  defmodule InfoDefinitionList do
    @derive [Poison.Encoder]
    defstruct [type: :definition_list, tags: [], items: []]
  end

  defmodule InfoLink do
    @derive [Poison.Encoder]
    defstruct [type: :link, tags: [], url: nil, text: "", window: false]
  end

  defmodule InfoGraph do
    @derive [Poison.Encoder]
    defstruct [type: :graph, tags: [], name: nil, text: ""]
  end

  defmodule InfoCerfa do
    @derive [Poison.Encoder]
    defstruct [type: :cerfa, tags: [], id: nil, notice_id: nil, text: ""]
  end

  defmodule InfoCall do
    @derive [Poison.Encoder]
    defstruct [type: :call, tags: [], module: nil, method: nil, args: []]
  end

  defmodule InfoError do
    @derive [Poison.Encoder]
    defstruct [type: :error, tags: [], text: nil]
  end

  def start_link do
    GraphEvents.stream_async(fn %GraphEvents.Event{event: e} ->
      if e in [:NODE_UPDATED, :NODE_ADDED, :NODE_DELETED,
               :EDGE_UPDATED, :EDGE_ADDED, :EDGE_DELETED] do
        Graph.reload
      end
    end)
    Agent.start_link(fn -> graph() end, name: __MODULE__)
  end

  def get do
    Agent.get(__MODULE__, fn graph -> graph end)
  end

  def reload do
    Logger.debug("Graph reloaded")
    Agent.update(__MODULE__, fn _old -> graph() end)
  end

  def graph do
    dag = :digraph.new([:acyclic])
    GraphDB.nodes
    |> Enum.each(fn n = %Graph.N{id: id} ->
      :digraph.add_vertex(dag, id, n)
    end)
    GraphDB.edges
    |> Enum.each(fn %Graph.E{source: s, target: t, context: c} ->
      :digraph.add_edge(dag, s, t, c)
    end)
    dag
  end

  def vertices(dag) do
    :digraph.vertices(dag)
  end

  def edges(dag) do
    :digraph.edges(dag)
  end

  def vertex_info(vertex, dag) do
    {_, info} = :digraph.vertex(dag, vertex)
    %N{info | id: vertex}
  end

  def edge_info(edge, dag) do
    {_, v1, v2, c} = :digraph.edge(dag, edge)
    case c do
      [] -> %E{source: v1, target: v2}
      _ -> %E{source: v1, target: v2, context: c}
    end
  end

  defp out_vertex_info(edge, dag) do
    {_, _, vertex, _} = :digraph.edge(dag, edge)
    vertex |> vertex_info(dag)
  end

  defp validate_edge_metadata({k, "#" <> v}, context) do
    bindings =
      Keyword.new()
      |> Keyword.put_new(String.to_atom(k), Map.get(context, k))
    {r, _} = Code.eval_string(v, bindings)
    r
  end

  defp validate_edge_metadata({k, v}, context) do
    Map.get(context, k) == v
  end

  defp validate_edge_metadatas(edge, context, metadata) do
    validate =
      metadata
      |> Enum.all?(fn m -> validate_edge_metadata(m, context) end)
    case validate do
      true ->
        {:ok, edge}
      _ ->
        {:error, "Context does not match edge metadata"}
    end
  end

  defp validate_context(context, %N{expect: %NodeValues{values: values}}) do
    values
    |> Enum.reduce([], fn(%NodeValue{name: name, default: default}, errors) ->
      case context
           |> Map.put_new(name, default)
           |> Map.get(name) do
        nil ->
          ["Missing value for #{name}" | errors]
        _ ->
          errors
      end
    end)
  end

  defp validate_vertex_info(info = %N{}, context) do
    case validate_context(context, info) do
      [] ->
        {:ok, info}
      errors ->
        {:error, errors}
    end
  end

  defp validate_edge(edge, dag, context) do
    {_, v1, v2, metadata} = :digraph.edge(dag, edge)
    Logger.debug("Validating edge #{v1} -> #{v2}")
    case metadata do
      [] ->
        {:ok, edge}
      %{} ->
        validate_edge_metadatas(edge, context, metadata)
      _ ->
        {:ok, edge}
    end
  end

  defp validate_edges(edges, dag, context) do
    edges
    |> Enum.map(&validate_edge(&1, dag, context))
    |> Enum.reduce(%{ok: [], error: []}, fn({k, v}, acc) ->
      case k do
        :ok -> %{acc | ok: [v | acc.ok]}
        :error -> %{acc | error: [v | acc.error]}
      end
    end)
  end

  #defp validate_vertices_info(infos, context) do
    #infos
    #|> Enum.map(&validate_vertex_info(&1, context))
    #|> Enum.reduce(%{ok: [], error: []}, fn({k, v}, acc) ->
      #case k do
        #:ok -> %{acc | ok: [v | acc.ok]}
        #:error -> %{acc | error: [v | acc.error]}
      #end
    #end)
  #end

  defp context_val(k, context) when is_atom(k) do
    Map.get(context, k, k)
  end

  defp context_val(k, _), do: k

  defp populate_info(info = %Api.Endpoint{params: params, path: path}, context) do
    # populate info of type %Api.Endpoint
    # params is a Keyword list, we transform it to a Map with
    # the values we find in the context
    params = params
    |> Enum.into(Map.new, fn(k) ->
      {k, context_val(k, context)}
    end)
    # path is a Keyword list, we replace the items
    # with the values we find in the context
    path = path
    |> Enum.map(&context_val(&1, context))
    # finally return the populated %Api.Endpoint
    %Api.Endpoint{info | params: params, path: path}
  end

  defp populate_info(%InfoCall{module: Book.Justice, args: args}, context) do
    loc = %Location.Commune{name: context["name"], zip_code: context["zip_code"]}
    case apply(Book.Justice, :get, [loc | args]) do
      {:ok, [res | _]} ->
        res
      {:ok, _} ->
        Logger.error("Failed to find #{hd(args)} for #{inspect(loc)}")
        %InfoError{text: "Aucun Tribunal trouvé pour cette localité."}

      {:error, errors} when is_map(errors) ->
        case Map.has_key?(errors, "territory") do
          true ->
            Logger.error("Failed to find #{inspect(loc)} in Book.Justice")
            %InfoError{text: "Aucun Tribunal trouvé pour cette localité. Sélectionnez
                              une autre ville aux alentours."}
          false ->
            error_text =
              errors
              |> Enum.reduce("", fn {_, v}, t ->
                v <> "\n" <> t
              end)
            %InfoError{text: error_text}
        end
      {:error, err} ->
        Logger.error("Failed to query Book.Justice: #{inspect(err)}")
        %InfoError{text: "Erreur lors de la récupération des coordonées.
                          Veuillez réessayer plus tard."}
    end
  end

  defp populate_info(info, _context), do: info

  defp populate_infos(info = %N{infos: infos}, context) do
    # given the list of infos
    # try to populate them with values from the context
    infos = infos
    |> Enum.map(&populate_info(&1, context))
    %N{info | infos: infos}
  end

  defp validate_vertex(vertex, dag, context) do
    vertex
    |> vertex_info(dag)
    |> validate_vertex_info(context)
  end

  def next(vertex, dag, context) do
    Logger.debug("I'm at vertex #{vertex}")
    Logger.debug("Context is #{inspect(context)}")

    case validate_vertex(vertex, dag, context) do
      {:ok, _} ->
        next_edges(vertex, dag, context)
      {:error, error} ->
        {:error, error}
    end
  end

  defp next_edges(vertex, dag, context) do
    # validate next edges
    edges =
      dag
      |> :digraph.out_edges(vertex)
      |> validate_edges(dag, context)

    case edges do
      # no more edges
      %{ok: [], error: []} ->
        {:ok, []}
      # errors in edge validation
      %{ok: [], error: errors} ->
        {:error, errors}
      %{ok: edges} ->
        edges
        |> Enum.map(&out_vertex_info(&1, dag))
        |> next_vertices(dag, context)
    end
  end

  defp next_vertices(infos, _dag, context) do
    {:ok, infos |> Enum.map(&populate_infos(&1, context))}
  end

end
