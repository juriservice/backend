defmodule Juriservice do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    alias Plug.Adapters.Cowboy

    children = [
      worker(Location.Sup, []),
      worker(GraphSup, []),
      worker(CerfaClean, []),
      Cowboy.child_spec(:http, ApiRouter, [], [port: 4001]),
      Cowboy.child_spec(:http, ApiAdminRouter, [], [port: 4002])
    ]

    opts = [strategy: :one_for_one, name: Juriservice.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
