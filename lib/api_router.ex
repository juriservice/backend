defmodule ApiRouter do
  use Plug.Router
  use Api.Helpers

  plug :match 
  plug :dispatch

  forward "/cerfa", to: CerfaRouter
  forward "/location", to: Location.Router
  forward "/graph", to: GraphRouter
  forward "/book", to: Book.Router

  match _ do
    send_resp(conn, 404, @not_found)
  end

end
