defmodule GraphDecode do

  defimpl Poison.Decoder, for: Graph.N do
    def decode(node_info, _options) do
      node_info
      |> Map.put(:type, String.to_atom(node_info.type))
      |> Map.update!(:infos, fn infos ->
        Enum.map(infos, &GraphDecode.transform_info/1)
      end)
      |> Map.update!(:expect, fn expect ->
        values =
          Map.get(expect, :values)
          |> Enum.map(&Poison.Decode.decode(&1, as: %Graph.NodeValue{}))
        %{expect | values: values}
      end)
    end
  end

  defimpl Poison.Decoder, for: Graph.E do
    def decode(edge_info, _options) do
      edge_info
      |> Map.update!(:context, fn context ->
        Enum.map(context, &GraphDecode.transform_context/1)
        |> Map.new
      end)
    end
  end

  def decode_edge(json) when is_binary(json) do
    json
    |> Poison.Parser.parse!
    |> Poison.Decode.decode(as: %Graph.E{})
  end

  def transform_context({k, v}) when is_binary(v) do
    case Poison.Parser.parse(v) do
      {:ok, t} ->
        {k, t}
      _ ->
        {k, v}
    end
  end

  def transform_context({k, v}), do: {k, v}

  def decode_node(json) when is_binary(json) do
    json
    |> Poison.decode!(as: %Graph.N{expect: %Graph.NodeValues{}})
  end

  def transform_info(%{"type" => type} = info) do
    info
    |> Map.delete("type")
    |> Map.put(:type, String.to_atom(type))
    |> decode_info
  end

  def transform_info(info) when is_binary(info) do
    try do
      String.to_existing_atom(info)
    rescue
      ArgumentError -> info
    end
  end

  def decode_info(%{type: :text} = info) do
    Poison.Decode.decode(info, as: %Graph.InfoText{})
  end

  def decode_info(%{type: :link} = info) do
    Poison.Decode.decode(info, as: %Graph.InfoLink{})
  end

  def decode_info(%{type: :note} = info) do
    Poison.Decode.decode(info, as: %Graph.InfoNote{})
  end

  def decode_info(%{type: :graph} = info) do
    Poison.Decode.decode(info, as: %Graph.InfoGraph{})
  end

  def decode_info(%{type: :call} = info) do
    args =
      info
      |> Map.get("args")
      |> Enum.map(&transform_info/1)
    info = %{info | "args" => args}
    info = %{info | "module" => String.to_existing_atom(info["module"])}
    Poison.Decode.decode(info, as: %Graph.InfoCall{})
  end

  def decode_info(%{type: :cerfa} = info) do
    Poison.Decode.decode(info, as: %Graph.InfoCerfa{})
  end

  def decode_info(%{type: :error} = info) do
    Poison.Decode.decode(info, as: %Graph.InfoError{})
  end

  def decode_info(%{type: :list} = info), do: decode_list(info, %Graph.InfoList{})

  def decode_info(%{type: :definition_list} = info), do: decode_list(info, %Graph.InfoDefinitionList{})

  def decode_info(%{type: :input} = info) do
    Poison.Decode.decode(info, as: %Graph.Input{})
  end

  def decode_info(%{type: :radio} = info) do
    Poison.Decode.decode(info, as: %Graph.Radio{choices: [%Graph.RadioChoice{}]})
  end

  def decode_info(%{type: :book_small} = info) do
    Poison.Decode.decode(info, as: %Book.InfoSmall{})
  end

  def decode_info(%{type: :book} = info) do
    Poison.Decode.decode(info, as: %Book.Info{})
  end

  def decode_info(info), do: info

  defp decode_list(info, type) do
    items =
      info
      |> Map.get("items")
      |> Enum.map(&transform_info/1)
    Poison.Decode.decode(%{info | "items" => items}, as: type)
  end

end
