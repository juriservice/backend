defmodule GraphRouter do
  use Plug.Router
  use Api.Helpers
  require Logger

  plug Plug.Parsers, parsers: [:urlencoded]
  plug :match
  plug :json_header_plug
  plug PlugCors
  plug :dispatch

  get "/:vertex/next" do
    dag = Graph.get
    case vertex in Graph.vertices(dag) do
      false ->
        send_resp(conn, 404, @not_found)
      true ->
        next_vertex(conn, vertex, dag)
    end
  end

  defp next_vertex(conn, vertex, dag) do
    context = conn.params
    case Graph.next(vertex, dag, context) do
      {:ok, data} ->
        send_resp(conn, 200, %Api.Response{data: data} |> Poison.encode!)
      {:error, error} ->
        send_resp(conn, 400, %Api.Response{status: 400, error: error} |> Poison.encode!)
    end
  end

  match _ do
    send_resp(conn, 404, @not_found)
  end

end
