defmodule GraphDB do
  require Logger
  use GenServer
  alias GraphEvents.Event

  @table_nodes :nodes
  @table_edges :edges

  @backend Application.get_env(:juriservice, GraphDB)
           |> Keyword.get(:backend, :ets)

  def start_link() do
    srv = GenServer.start_link(__MODULE__, [], name: __MODULE__)
    with {:ok, _} <- srv,
         :ok <- load_fixtures() do
      srv
    else
      {:error, r} ->
        Logger.error("Error when starting GraphDB: #{inspect(r)}")
        srv
    end
  end

  defp load_fixtures do
    fixtures =
      Application.get_env(:juriservice, GraphDB)
      |> Keyword.get(:fixtures, nil)

    case fixtures do
      nil ->
        :ok
      _ ->
        Path.join(:code.priv_dir(:juriservice), fixtures)
        |> GraphDB.import_json
    end
  end

  def stop() do
    GenServer.stop(__MODULE__)
  end

  def init([]) do
    case @backend do
      :dets ->
        :dets.open_file(@table_nodes, [file: :"nodes.db"])
        :dets.open_file(@table_edges, [file: :"edges.db"])
      :ets ->
        :ets.new(@table_nodes, [:set, :named_table, {:read_concurrency, true}])
        :ets.new(@table_edges, [:set, :named_table, {:read_concurrency, true}])
    end
    {:ok, nil}
  end

  defp exists(%Graph.N{id: id}) do
    @backend.member(@table_nodes, id)
  end

  defp exists(%Graph.E{id: id}) do
    @backend.member(@table_edges, id)
  end

  defp insert_item(n = %Graph.N{id: id}) do
    event =
      case exists(n) do
        true -> :NODE_UPDATED
        false -> :NODE_ADDED
      end
    case @backend.insert(@table_nodes, {id, n}) do
      # dets return :ok while ets return true
      r when r in [true, :ok] ->
        GraphEvents.notify(%Event{event: event, id: id, obj: n})
        :ok
      err -> err
    end
  end

  defp insert_item(e = %Graph.E{id: id, source: s, target: t}) do
    event =
      case exists(e) do
        true -> :EDGE_UPDATED
        false -> :EDGE_ADDED
      end
    case @backend.insert(@table_edges, {id, s, t, e}) do
      r when r in [true, :ok] ->
        GraphEvents.notify(%Event{event: event, id: id, obj: e})
        :ok
      {:error, r} -> {:error, r}
    end
  end

  defp delete_item(%Graph.N{id: id}) do
    case @backend.delete(@table_nodes, id) do
      r when r in [true, :ok] ->
        GraphEvents.notify(%Event{event: :NODE_DELETED, id: id})
        :ok
      {:error, r} -> {:error, r}
    end
  end

  defp delete_item(%Graph.E{id: id}) do
    case @backend.delete(@table_edges, id) do
      r when r in [true, :ok] ->
        GraphEvents.notify(%Event{event: :EDGE_DELETED, id: id})
        :ok
      {:error, r} -> {:error, r}
    end
  end

  def handle_call({:delete, table}, _, s) when is_atom(table) do
    {:reply, @backend.delete_all_objects(table), s}
  end

  def handle_call({:delete, item}, _, s) do
    {:reply, delete_item(item), s}
  end

  def handle_call({:insert, item}, _, s) do
    {:reply, insert_item(item), s}
  end

  def delete(item) do
    GenServer.call(__MODULE__, {:delete, item})
  end

  def insert(item) do
    GenServer.call(__MODULE__, {:insert, item})
  end

  def terminate(_, _) do
    case @backend do
      :dets ->
        :dets.close(@table_nodes)
        :dets.close(@table_edges)
      _ -> nil
    end
  end

  def node(id) do
    nodes =
      @table_nodes
      |> @backend.match({id, :"$1"})
      |> List.flatten
    case nodes do
      [] -> {:error, :notfound}
      [n] -> {:ok, n}
    end
  end

  def nodes do
    @table_nodes
    |> @backend.match({:_, :"$1"})
    |> List.flatten
  end

  def edge(id) do
    edges =
      @table_edges
      |> @backend.match({id, :_, :_, :"$1"})
      |> List.flatten
    case edges do
      [] -> {:error, :notfound}
      [e] -> {:ok, e}
    end
  end

  def edges do
    @table_edges
    |> @backend.match({:_, :_, :_, :"$1"})
    |> List.flatten
  end

  def edges({:source, id}) do
    @table_edges
    |> @backend.match({:_, id, :_, :"$1"})
    |> List.flatten
  end

  def edges({:target, id}) do
    @table_edges
    |> @backend.match({:_, :_, id, :"$1"})
    |> List.flatten
  end

  def graph do
    # sort nodes so that diffing is easy
    %Graph.G{nodes: nodes() |> Enum.sort(fn n1, n2 -> n1.id <= n2.id end),
             edges: edges() |> Enum.sort(fn n1, n2 -> n1.id <= n2.id end)}
  end

  def json(graph) do
    graph
    |> Poison.encode_to_iodata(pretty: true)
  end

  def dump_json(file_path) do
    with {:ok, data} <- graph() |> json(),
         :ok <- File.write(file_path, data)
    do
      :ok
    else
      {:error, r} -> {:error, r}
    end
  end

  def import_json(file_path) do
    with {:ok, json} <- File.read(file_path),
         {:ok, graph} <- Poison.decode(json, as: %Graph.G{nodes: [%Graph.N{}], edges: [%Graph.E{}]})
    do
      GraphDB.delete(@table_nodes)
      graph.nodes
      |> Enum.each(&insert/1)
      GraphDB.delete(@table_edges)
      graph.edges
      |> Enum.each(&insert/1)
      :ok
    else
      {:error, r} -> {:error, r}
    end
  end

end
