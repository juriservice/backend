defmodule GraphEvents do
  use GenEvent
  require Logger

  defmodule Event do
    @derive [Poison.Encoder]
    defstruct [:event, :id, :obj]
  end

  defmodule Stream do
    defstruct [:task]

    def wait(s = %Stream{task: t}) do
      case Task.yield(t, 1000) do
        nil ->
          wait(s)
        {:ok, r} ->
          r
      end
    end
  end

  def start_link() do
    GenEvent.start_link(name: __MODULE__)
  end

  def notify(e = %Event{}) do
    GenEvent.notify(__MODULE__, e)
  end

  def stream(target) do
    Logger.debug("Streaming graph events to #{inspect(target)}")
    GenEvent.stream(__MODULE__)
      |> Enum.reduce_while(target, fn e, target ->
      send_event(target, e)
    end)
  end

  def stream_async(target) do
    task = Task.Supervisor.async_nolink(GraphEvents.Tasks,
                                        fn -> stream(target) end)
    %Stream{task: task}
  end

  defp send_event(pid, e = %Event{}) when is_pid(pid) do
    case Process.alive?(pid) do
      true ->
        send(pid, e)
        {:cont, pid}
      false ->
        {:halt, pid}
    end
  end

  defp send_event(conn = %Plug.Conn{}, e = %Event{}) do
    case Plug.Conn.chunk(conn, "event: \"message\"\n\ndata: #{e |> Poison.encode!}\n\n") do
      {:ok, conn} ->
        {:cont, conn}
      {:error, :closed} ->
        {:halt, conn}
    end
  end

  defp send_event(func, e = %Event{}) when is_function(func) do
    case func.(e) do
      :ok ->
        {:cont, func}
      _ ->
        {:halt, func}
    end
  end

  defp send_event(t, _) do
    IO.inspect("Skip event, not supported target : #{inspect(t)}")
  end

end
