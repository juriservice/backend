defmodule GraphAdminRouter do
  use Plug.Router
  use Api.Helpers
  require Logger

  plug Plug.Logger, log: :debug
  plug Plug.Parsers, parsers: [:urlencoded],
                     pass:  ["*/*"]
  plug Plug.Static, at: "/",
                    from: :juriservice
  plug :match
  plug :json_header_plug
  plug :dispatch

  get "/graph" do
    g = %Graph.G{
      nodes: GraphDB.nodes,
      edges: GraphDB.edges
    }
    send_resp(conn, 200, g |> Poison.encode!)
  end

  get "/graph/events/" do
    conn
    |> put_resp_header("content-type", "text/event-stream")
    |> send_chunked(200)
    |> GraphEvents.stream_async
    |> GraphEvents.Stream.wait
  end

  get "/graph/edge/" do
    send_resp(conn, 200, GraphDB.edges |> Poison.encode!)
  end

  get "/graph/edge/:id" do
    case GraphDB.edge(id) do
      {:ok, edge} ->
        send_resp(conn, 200, edge |> Poison.encode!)
      {:error, reason} ->
        response(conn, 404, reason)
    end
  end

  put "/graph/edge/:id" do
    with {:ok, body, _} <- read_body(conn),
         edge <- body |> GraphDecode.decode_edge,
         true <- edge.id == id,
         :ok <- GraphDB.insert(edge)
    do
      send_resp(conn, 204, "")
    else
      false -> response(conn, 400, "Wrong edge ID")
      {:error, reason} -> response(conn, 500, reason)
    end
  end

  delete "/graph/edge/:id" do
    case GraphDB.delete(%Graph.E{id: id}) do
      :ok ->
        send_resp(conn, 204, "")
      {:error, reason} ->
        response(conn, 500, reason)
    end
  end

  post "/graph/edge/" do
    id = UUID.uuid4()
    with {:ok, body, _} <- read_body(conn),
         edge = body
                |> GraphDecode.decode_edge
                |> Map.put(:id, id),
         :ok <- GraphDB.insert(edge)
    do
        send_resp(conn, 200, edge |> Poison.encode!)
    else
      {:error, reason} ->
        response(conn, 500, reason)
    end
  end

  get "/graph/node/" do
    nodes =
      GraphDB.nodes
      |> Enum.map(fn n ->
        # TODO: set order in all nodes
        %{id: n.id, title: n.title, order: Map.get(n, :order, 0)}
      end)
    send_resp(conn, 200, nodes |> Poison.encode!)
  end

  get "/graph/node/:id" do
    case GraphDB.node(id) do
      {:ok, node} ->
        send_resp(conn, 200, node |> Poison.encode!)
      {:error, reason} ->
        response(conn, 404, reason)
    end
  end

  put "/graph/node/:id" do
    with {:ok, body, _} <- read_body(conn),
         node <- body |> GraphDecode.decode_node,
         true <- node.id == id,
         :ok <- GraphDB.insert(node)
    do
      send_resp(conn, 204, "")
    else
      false -> response(conn, 400, "Wrong node ID")
      {:error, reason} -> response(conn, 500, reason)
    end
  end

  delete "/graph/node/:id" do
    case GraphDB.delete(%Graph.N{id: id}) do
      :ok ->
        send_resp(conn, 204, "")
      {:error, reason} ->
        response(conn, 500, reason)
    end
  end

  post "/graph/node/" do
    id = UUID.uuid4()
    with {:ok, body, _} <- read_body(conn),
         node = body
                |> GraphDecode.decode_node
                |> Map.put(:id, id),
         :ok <- GraphDB.insert(node)
    do
        send_resp(conn, 200, node |> Poison.encode!)
    else
      {:error, reason} ->
        response(conn, 500, reason)
    end
  end

  match _ do
    response(conn, 404, "Not found")
  end

end
