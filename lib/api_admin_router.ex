defmodule ApiAdminRouter do
  use Plug.Router
  use Api.Helpers

  plug :match 
  plug :dispatch

  forward "/admin", to: GraphAdminRouter

  match _ do
    send_resp(conn, 404, @not_found)
  end

end
