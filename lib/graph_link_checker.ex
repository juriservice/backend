defmodule GraphLinkChecker do
  use GenServer
  require Logger

  alias Graph.N

  @interval 60_000 * 60 * 6

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init([]) do
    config = :juriservice
      |> Application.get_env(__MODULE__)
      |> Map.new
    GraphEvents.stream_async(self())
    :timer.send_after(5000, :check)
    {:ok, %{urls: extract_urls(), config: config}}
  end

  def handle_info(:check, %{urls: urls, config: config} = s) do
    # TODO: send mail or something
    case check_urls(urls, config) do
      [] -> :ok
      errors ->
        if Map.get(config, :mattermost_webhook) != nil do
          notify(config.mattermost_webhook, errors)
        else
          Logger.error(inspect(errors))
        end
    end
    :timer.send_after(@interval, :check)
    {:noreply, s}
  end

  def handle_info({:DOWN, _from, :process, _pid, reason}, s) do
    Logger.warn(reason)
    {:noreply, s}
  end

  def handle_info(%GraphEvents.Event{event: e}, s) do
    case e in [:NODE_UPDATED, :NODE_ADDED, :NODE_DELETED] do
      true ->
        {:noreply, %{s | urls: extract_urls()}}
      false ->
        {:noreply, s}
    end
  end

  defp notify(m_webhook, errors) do
    errors
    |> Enum.each(fn err ->
      body = err |> msg |> Poison.encode!
      HTTPoison.post(m_webhook, body, [{"Content-Type", "application/json"}])
    end)
  end

  defp msg({:error, {url, nil, err}}), do: %{text: ":bangbang: Error with URL #{url}: #{inspect(err)}"}
  defp msg({:error, {url, code, _err}}), do: %{text: ":bangbang: Error with URL #{url}, code #{code}"}
  defp msg({:warning, {url, code, _err}}), do: %{text: ":interrobang: Warning with URL #{url}, code #{code}"}
  defp msg(err), do: %{text: ":trollface: Unhandled error: #{inspect(err)}"}

  defp extract_urls do
    Graph.get
    |> graph_infos
    |> List.flatten
    |> Enum.reduce([], &extract_url/2)
    |> Enum.uniq
  end

  defp graph_infos(dag) do
    dag
    |> Graph.vertices
    |> Enum.into([], fn v ->
      Graph.vertex_info(v, dag)
    end)
    |> Enum.reduce([], fn %N{infos: infos}, acc ->
      infos ++ acc
    end)
    |> Enum.reduce([], &expand_info/2)
  end

  defp expand_info(%Graph.InfoList{items: items}, infos) do
    items ++ infos
  end

  defp expand_info(info, infos) do
    [info | infos]
  end

  defp extract_url(%Graph.InfoLink{url: url}, urls), do: [url | urls]

  defp extract_url(%Graph.InfoCerfa{} = i, urls), do: [i | urls]

  defp extract_url(_, urls), do: urls

  def filter_urls(urls, excludes) do
    urls
    |> Enum.filter(fn u ->
      !Enum.any?(excludes, fn e -> e == u end)
    end)
  end

  def check_urls(urls, config) do
    case Map.get(config, :excludes) do
        nil ->
          urls
        excludes ->
          urls |> filter_urls(excludes)
    end
    |> Enum.map(fn url ->
      Task.Supervisor.async(GraphLinkChecker.Tasks,
                            GraphLinkChecker, :check, [url])
    end)
    |> Enum.map(&Task.await/1)
    |> Enum.filter(fn {r, {_, _, _}} -> r != :ok end)
  end

  def check(%Graph.InfoCerfa{id: id}) do
    case CerfaRouter.cerfa_download(Integer.to_string(id)) do
      {:ok, _} ->
        {:ok, {CerfaRouter.cerfa_url(id), 200, nil}}
      {:error, _} ->
        {:error, {CerfaRouter.cerfa_url(id), 404, nil}}
    end
  end

  def check(url) do
    case HTTPoison.get(url) do
      {:ok, r} ->
        check_code(r, url)
      {:error, %HTTPoison.Error{reason: r}} ->
        {:error, {url, nil, r}}
    end
  end

  defp check_code(%HTTPoison.Response{status_code: code}, url) do
    cond do
      code > 400 ->
        {:error, {url, code, nil}}
      code > 300 ->
        {:warning, {url, code, nil}}
      true ->
        {:ok, {url, code, nil}}
    end
  end

end
