defmodule GraphDot do
  @type digraph_type :: {:digraph, integer, integer, integer, boolean()}

  @spec edge_label(%{}) :: String.t
  defp edge_label(m) when is_map(m) do
    m
    |> Enum.map(fn {k, v} ->
      "#{k} = #{v}"
    end)
    |> Enum.join(", ")
  end

  @spec edge_label([]) :: String.t
  defp edge_label([]), do: ""

  @spec to_dot(digraph_type) :: String.t
  def to_dot(dag) do
    nodes =
      dag
      |> :digraph.vertices
      |> Enum.map(fn(v) ->
        name = Atom.to_string(v)
        "#{name} [label=\"#{name}\"]"
      end)

    edges =
      dag
      |> :digraph.edges
      |> Enum.map(fn(e) ->
        {_, v1, v2, info} = :digraph.edge(dag, e)
        label = edge_label(info)
        "#{Atom.to_string(v1)} -> #{Atom.to_string(v2)} [label=\"#{label}\"]"
      end)

    "digraph G { #{Enum.join(nodes ++ edges, ";")}; }"
  end

  @spec to_png(String.t, String.t) :: {atom(), String.t}
  def to_png(dot_syntax, output_filename) do
    dot_syntax
    |> to_graphviz("png", output_filename)
  end

  @spec to_graphviz(String.t, String.t, String.t) :: {:ok, String.t}
  def to_graphviz(dot_syntax, format, output_filename) do
    {:ok, tmp_file} = create_tmp_file(dot_syntax)
    run_dot(tmp_file, output_filename, format)
    delete_tmp_file(tmp_file)
    {:ok, "#{output_filename}.#{format}"}
  end

  @spec create_tmp_file(String.t) :: {:ok, String.t}
  defp create_tmp_file(dot_syntax) do
    tmp_file = "tmp.#{:os.system_time(:seconds)}.dot"
    {:ok, file} = File.open tmp_file, [:write]
    :ok = IO.binwrite file, dot_syntax
    :ok = File.close file
    {:ok, tmp_file}
  end

  @spec run_dot(String.t, String.t, String.t) :: {Collectable.t, exit_status :: non_neg_integer}
  defp run_dot(input_file, output_filename, format) do
    System.cmd("dot", ["-T#{format}", "-o#{output_filename}.#{format}", input_file])
  end

  @spec delete_tmp_file(String.t) :: :ok
  defp delete_tmp_file(tmp_file), do: File.rm(tmp_file)

end
