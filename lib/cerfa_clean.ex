defmodule CerfaClean do
  require Logger
  use GenServer

  def init([]) do
    schedule()
    {:ok, []}
  end

  def start_link do
    GenServer.start_link(__MODULE__, [], [])
  end

  def handle_info(:clean, state) do
    clean()
    {:noreply, state}
  end

  defp schedule do
    %{clean_interval: interval} =
      :juriservice
      |> Application.get_env(Cerfa)
      |> Map.new
    :timer.send_after(interval, :clean)
  end

  defp compare_date(path) do
    %File.Stat{ctime: ctime} = File.stat!(path)
    {days, {_, _, _}} = :calendar.time_difference(ctime, :calendar.local_time)
    days > 5
  end

  defp delete_file(path) do
    Logger.debug("Deleting #{inspect(path)}")
    File.rm!(path)
  end

  def clean() do
    base_dir = Application.app_dir(:juriservice, "priv/cerfa")
    base_dir
    |> File.ls!
    |> Enum.map(&Path.join(base_dir, &1))
    |> Enum.filter(&compare_date/1)
    |> Enum.each(&delete_file/1)
    schedule()
  end

end
