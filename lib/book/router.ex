defmodule Book.Router do
  use Plug.Router
  use Api.Helpers

  plug Plug.Parsers, parsers: [:urlencoded]
  plug :match
  plug :json_header_plug
  plug PlugCors
  plug :dispatch

  defp response_type(conn) do
    case Map.get(conn.params, "type") do
      "small" -> :info_small
      _ -> :info
    end
  end

  for type <- Book.Justice.types do
    path = "/justice/" <> Atom.to_string(type)
    get path do
      params = conn.params
      commune = %Location.Commune{zip_code: params["zip_code"], name: params["name"]}
      resp = case Book.Justice.get(commune, unquote(type), response_type(conn)) do
        {:ok, data} ->
          %Api.Response{data: data}
        {:error, err} ->
          %Api.Error{error: err}
      end
      send_resp(conn, 200, resp |> Poison.encode!)
    end
  end

  match _ do
    send_resp(conn, 404, @not_found)
  end

end
