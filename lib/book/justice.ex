defmodule Book.Justice do
  require Logger

  @base_url "http://etalage.justice.comarquage.fr/api/v1/liste"

  @types %{
    ti: "Tribunal d'instance",
    tgi: "Tribunal de grande instance (TGI)",
    cph: "Conseil des Prud'hommes",
    tass: "Tribunal des affaires de la Sécurité sociale",
    caa: "Cour administrative d'appel",
    ca: "Cour d'appel (CA)",
    cass: "Cour d'assises",
    ta: "Tribunal administratif",
    tc: "Tribunal de commerce",
  }

  def types() do
    @types |> Map.keys
  end

  defp parse_field(f, info) do
    %{"id" => id, "value" => val, "label" => label} = f
    case {id, label} do
      {"name", _} ->
        %{info | name: val}
      {"url", _} ->
        %{info | url: val}
      {"adr", _} ->
        val
        |> Enum.reduce(info, fn(f2, info) ->
          parse_field(f2, info)
        end)
      {"street-address", _} ->
        %{info | address: info.address ++ String.split(val, "\n")}
      {"postal-distribution", _} ->
        %{info | address: info.address ++ String.split(val, "\n")}
      {"tel", _} ->
        %{info | phone: val}
      {"fax", _} ->
        %{info | fax: val}
      {"geo", _} ->
        [_, _, precision] = val
        cond do
          precision > 6 ->
            # [longitude, latitude]
            %{info | geo: Enum.take(val, 2) |> Enum.reverse}
          true ->
            info
        end
      {"email", _} ->
        %{info | email: val}
      {_, "Horaires d'ouverture"} ->
        %{info | opening_hours: [val]}
      _ ->
        info
    end
  end

  defp parse_item(item, info) do
    item["fields"]
    |> Enum.reduce(info, &parse_field/2)
  end

  defp parse_items(response, info) do
    response["data"]["items"]
    |> Enum.map(&parse_item(&1, info))
  end

  defp parse_resp(body, info) do
    result = body |> Poison.decode!
    case Map.has_key?(result, "errors") do
      false ->
        {:ok, result |> parse_items(info)}
      true ->
        {:error, result["errors"]}
    end
  end

  def get(%Location.Commune{zip_code: zip, name: name}, category, info_type \\ :info) do
    params = %{territory: "#{zip} #{name}", category: @types[category]}
    case @base_url |> URI.encode |> HTTPoison.get([], [params: params]) do
      {:ok, %HTTPoison.Response{body: body}} ->
        info =
          case info_type do
            :info -> %Book.Info{}
            :info_small -> %Book.InfoSmall{}
          end
        parse_resp(body, info)
      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}
    end
  end

end
