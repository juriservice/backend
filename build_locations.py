#!/usr/bin/env python
import re
import os.path
from collections import namedtuple

import requests


locs = {}
Loc = namedtuple('Loc', ['code', 'name', 'zip_code'])


def build():
    source = os.path.join("priv", "laposte_hexasmal.csv")
    dest = os.path.join("priv", "locations.csv")

    with open(source) as h:
        parse(h)

    with open(dest, 'w') as h:
        for name, infos in locs.items():
            loc = infos['loc']
            h.write("%s;%s;%s;\n" % (loc.code, loc.name, loc.zip_code))


def parse(h):
    for line in h.readlines():
        line = line.strip().split(';')
        loc = Loc(line[0], line[1], line[2])
        if re.match("^[0-9]+$", loc.name.split(' ')[-1]):
            check(loc)
        elif loc.name in locs:
            check(loc)
        else:
            locs[loc.name] = {
                'loc': loc,
                'fix': False
            }


def check(loc):
    if loc.name not in locs:
        print("Get %s" % str(loc))
        fix(loc)
    elif locs[loc.name]['loc'] == loc:
        print("Duplicate %s" % str(loc))
    elif locs[loc.name]['fix'] is False:
        print("Fix %s" % str(loc))
        fix(loc)


def fix(loc):
    base_url = "http://ou.comarquage.fr/api/v1/autocomplete-territory?kind=ArrondissementOfCommuneOfFrance&kind=CommuneOfFrance&kind=DelegatedCommuneOfFrance&term="
    url = base_url + loc.zip_code + " " + loc.name

    res = requests.get(url)
    items = res.json()["data"]["items"]
    if len(items) == 0:
        print("No results for %s" % str(loc))
    else:
        for item in items:
            zip_code, name = item["main_postal_distribution"].split(" ", 1)
            code = item["code"]
            if name in locs:
                locs[name]['loc'] = Loc(code, name, zip_code)
                locs[name]['fix'] = True
            else:
                locs[name] = {}
                locs[name]['loc'] = Loc(code, name, zip_code)
                locs[name]['fix'] = False

if __name__ == "__main__":
    build()
