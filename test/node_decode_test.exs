ExUnit.start

defmodule NodeDecodeTest do
  use ExUnit.Case, async: true

  test "decode node" do
    json = """
    {
      "id": "foo",
      "type": "info",
      "expect": {
        "values": [
          {
            "default": null,
            "name": "foo",
            "type": "string"
          }
        ]
      }
    }
    """

    e = %Graph.N{
      id: "foo",
      type: :info,
      expect: %Graph.NodeValues{
        values: [
          %Graph.NodeValue{
            name: "foo",
            type: "string"
          }
        ]
      }
    }

    r = GraphDecode.decode_node(json)

    assert r == e
  end

  test "decode edge" do
    json = """
    {
      "source": "foo",
      "target": "bar",
      "context": {
        "name": "foo"
      }
    }
    """

    e = %Graph.E{source: "foo", target: "bar",
                        context: %{"name" => "foo"}}

    r = GraphDecode.decode_edge(json)

    assert r == e
  end

  test "text info decoding" do

    json = """
    {
      "id": "foo",
      "type": "info",
      "infos": [
        {
          "type": "text",
          "text": "bar"
        }
      ]
    }
    """

    e = %Graph.N{id: "foo", type: :info, infos: [
      %Graph.InfoText{text: "bar"}
    ]}
    r = GraphDecode.decode_node(json)

    assert r == e
  end

  test "input info decoding" do

    json = """
    {
      "id": "foo",
      "type": "choice",
      "infos": [
        {
          "type": "input",
          "label": "bar",
          "name": "bar"
        }
      ]
    }
    """

    e = %Graph.N{id: "foo", type: :choice, infos: [
      %Graph.Input{label: "bar", name: "bar"}
    ]}
    r = GraphDecode.decode_node(json)

    assert r == e
  end

  test "input radio decoding" do

    json = """
    {
      "id": "foo",
      "type": "choice",
      "infos": [
        {
          "type": "radio",
          "label": "bar",
          "name": "bar",
          "choices": [
            {
              "key": "foo",
              "name": "foo",
              "desc": "foo"
            },
            {
              "key": "bar",
              "name": "bar",
              "desc": null
            }
          ]
        }
      ]
    }
    """

    e = %Graph.N{id: "foo", type: :choice, infos: [
      %Graph.Radio{label: "bar", name: "bar", choices: [
        %Graph.RadioChoice{key: "foo", name: "foo", desc: "foo"},
        %Graph.RadioChoice{key: "bar", name: "bar"}
      ]}
    ]}
    r = GraphDecode.decode_node(json)

    assert r == e
  end

end
