ExUnit.start

defmodule GraphTest do
  use ExUnit.Case, async: true
  alias Graph.NodeValues
  alias Graph.NodeValue

  test "next node after root" do
    dag = Graph.get
    result = Graph.next("root", dag, %{})
    expected = %Graph.N{
      id: "a",
      title: "A",
      type: :choice,
      expect: %NodeValues{
        values: [
          %NodeValue{name: "val", type: "integer", default: nil},
          %NodeValue{name: "val2", type: "string", default: nil},
        ]
      }
    }
    assert result == {:ok, [expected]}
  end

  test "missing context value returns error" do
    dag = Graph.get

    {:error, [error1, error2]} = Graph.next("a", dag, %{})
    assert error1 == "Missing value for val2"
    assert error2 == "Missing value for val"
  end

  test "without context value" do
    dag = Graph.get

    {:ok, [info]} = Graph.next("b", dag, %{})
    assert info.id == "d"
  end

  test "with context value" do
    dag = Graph.get

    {:ok, [info1, info2]} = Graph.next("a", dag, %{"val" => 20, "val2" => ""})
    assert info1.id == "e"
    assert info2.id == "b"

    {:ok, [info1, info2]} = Graph.next("a", dag, %{"val" => 10, "val2" => ""})
    assert info1.id == "c"
    assert info2.id == "d"
    
    {:ok, [info1]} = Graph.next("a", dag, %{"val" => 0, "val2" => "foobar"})
    assert info1.id == "f"
  end

  test "last node returns empty list" do
    dag = Graph.get
    {:ok, []} = Graph.next("d", dag, %{})
  end
end
