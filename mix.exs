defmodule Juriservice.Mixfile do
  use Mix.Project

  def project do
    [app: :juriservice,
     version: "0.1.0",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger, :httpoison, :cowboy, :plug],
     included_applications: [:poison, :trie, :uuid, :plug_cors],
     mod: {Juriservice, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [{:httpoison, "~> 1.0.0"},
     {:poison, "~> 3.1.0"},
     {:plug, "~> 1.0"},
     {:plug_cors, "~> 0.8.0"},
     {:cowboy, "~> 1.0.0"},
     {:distillery, "~> 1.5.0"},
     {:trie, "~> 1.7.0"},
     {:uuid, "~> 1.1"}
    ]
  end
end
