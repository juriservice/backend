use Mix.Config

config :juriservice, GraphDB,
  backend: :ets,
  fixtures: "test.json"
