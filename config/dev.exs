use Mix.Config

config :logger, level: :debug

config :juriservice, GraphDB,
  backend: :ets,
  fixtures: "content/graph.json"
