use Mix.Config

config :logger, level: :info

config :plug_cors,
  origins: ["*"],
  methods: ["GET"],
  headers: ["Authorization"]

config :juriservice, :locations, "locations.csv"

config :juriservice, Cerfa,
  clean_interval: 1000 * 60 * 60 * 24 * 15

config :juriservice, GraphLinkChecker,
  mattermost_webhook: System.get_env("JURISERVICE_MATTERMOST_HOOK"),
  excludes: ["https://www.cnb.avocat.fr/annuaire-des-avocats-de-france", "https://www.pagesjaunes.fr"]

import_config "#{Mix.env}.exs"
