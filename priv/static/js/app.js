/*global angular */
/*global console */

'use strict';

angular.module('app', [
  'utils',
  'node',
  'infos',
  'api',
  'tree',
  'ngResource',
  'as.sortable',
  'ui.bootstrap',
  'bsLoadingOverlay',
])

.run(function(bsLoadingOverlayService, actions) {
	bsLoadingOverlayService.setGlobalConfig({
		templateUrl: 'partials/loading-overlay-template.html'
	});
})


.filter('nodeTitle', ['api', function(api) {

  return function(id) {
    return api.getNode(id).title;
  };

}])

.factory('typeFactory', function() {

  return {
    string: function() {
      return "";
    },
    nodeValue: function() {
      return angular.copy({type: '', name: '', default: null});
    }
  };

})

.constant('infoTypes', {
  text: {
    name: 'Text info',
    skel: {type: "text", tags: [], text: ""}
  },
  link: {
    name: 'Link info',
    skel: {type: "link", tags: [], text: "", url: "", window: false}
  },
  cerfa: {
    name: 'Cerfa info',
    skel: {type: "cerfa", tags: [], cerfa_id: null, notice_id: null, text: ""}
  },
  list: {
    name: 'List',
    skel: {type: "list", tags: [], items: []}
  },
  definition_list: {
    name: 'Definition List',
    skel: {type: "definition_list", tags: [], items: []}
  },
  note: {
    name: 'Note info',
    skel: {type: "note", tags: [], text: ""}
  },
  graph: {
    name: 'Graph info',
    skel: {type: "graph", tags: []}
  },
  call: {
    name: 'Call info',
    skel: {type: "call", tags: [], args: []}
  },
  radio: {
    name: 'Radio choice',
    skel: {type: "radio", tags: [], choices: []}
  },
  input: {
    name: 'Input choice',
    skel: {type: "input", tags: []}
  }
})

.controller('globalCtrl', function globalCtrl($scope, $timeout, api, tree, events, bsLoadingOverlayService) {
  var self = this;

  self.rootNodes = tree.rootNodes;
  self.node = null;

  self.reload = function() {
    api.reload();
  };

  self.newNode = function() {
    var node = new api.Node({
      type: "info",
      infos: []
    });
    $scope.$broadcast(events.NODE_SHOW, node);
  };

  $scope.$on(events.NODE_DELETED, function(event, node) {
    if (self.node && self.node.id == node.id) {
      self.node = null;
    }
  });

  $scope.$on(events.NODE_SHOW, function(event, node) {
    if (node.id) {
      bsLoadingOverlayService.start();
      api.fetchNode(node).then(
        function(node) {
          self.node = node;
          bsLoadingOverlayService.stop();
        }
      );
    }
    else {
      self.node = node;
    }

  });

})

.directive('listForm', function() {

  return {
    templateUrl: 'partials/generics/list.html',
    restrict: 'E',
    scope: {
      name: '@',
      items: '=',
      itemType: '@',
      itemSkel: '&'
    },
    controller: ['$scope', function($scope) {
      var self = $scope;

      self.add = function() {
        if (self.items === null)
          self.items = [];
        self.items.push(self.itemSkel()());
      };
      self.remove = function(index) {
        self.items.splice(index, 1);
      };

    }]
  };
})

.directive('objectForm', function() {

  return {
    templateUrl: 'partials/generics/object.html',
    restrict: 'E',
    scope: {
      obj: '=',
      skel: '&'
    }
  };

});
