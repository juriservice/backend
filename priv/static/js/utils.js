/*global angular */
/*global console */

'use strict';

angular.module('utils', [])

.factory('RecursionHelper', ['$compile', function($compile){
    return {
        /**
         * Manually compiles the element, fixing the recursion loop.
         * @param element
         * @param [link] A post-link function, or an object with function(s) registered via pre and post properties.
         * @returns An object containing the linking functions.
         */
        compile: function(element, link){
            // Normalize the link parameter
            if(angular.isFunction(link)){
                link = { post: link };
            }

            // Break the recursion loop by removing the contents
            var contents = element.contents().remove();
            var compiledContents;
            return {
                pre: (link && link.pre) ? link.pre : null,
                /**
                 * Compiles and re-adds the contents
                 */
                post: function(scope, element){
                    // Compile the contents
                    if(!compiledContents){
                        compiledContents = $compile(contents);
                    }
                    // Re-add the compiled contents to the element
                    compiledContents(scope, function(clone){
                        element.append(clone);
                    });

                    // Call the post-linking function, if any
                    if(link && link.post){
                        link.post.apply(null, arguments);
                    }
                }
            };
        }
    };
}])

.factory('dialogs', function($q) {

  var popups = [];

  function Dialog(params) {
    angular.extend(this, params);
    this.result = $q.defer();
    popups.push(this);
  }

  Dialog.prototype = {

    close: function(value) {
      popups.splice(popups.indexOf(this), 1);
      if (value)
        this.result.resolve(value);
      else {
        this.result.notify("aborted");
        this.result.reject(value);
      }
    },

    canConfirm: function() {
      return ["confirm", "info"].indexOf(this.type) !== -1;
    },

    canAbort: function() {
      return ["confirm"].indexOf(this.type) !== -1;
    }
  };

  return {

    all: function() {
      return popups;
    },

    show: function(type, klass, title, body, force) {
      var d = new Dialog({
        type: type,
        klass: klass,
        title: title,
        body: body
      });
      if (force) {
        d.close(force);
      }
      return d.result.promise;
    },

    confirm: function(title, body, force) {
      return this.show('confirm', 'info', title, body, force);
    },

    success: function(title, body) {
      return this.show('info', 'success', title, body);
    },

    error: function(title, body) {
      return this.show('info', 'danger', title, body);
    },

    modal: function(title, body) {
      return this.show('modal', 'info', title, body);
    },

    close: function(force) {
      var dialog = popups[popups.length - 1];
      if (dialog.type != 'modal' || force === true)
        dialog.close(false);
    }
  };
})

.directive('dialogs', function() {

  return {
    restrict: 'E',
    templateUrl: 'partials/dialogs.html',
    controller: ['$scope', 'dialogs', function($scope, dialogs) {
      $scope.dialogs = dialogs;
    }]
  };

});

