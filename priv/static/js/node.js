/*global angular */
/*global console */

'use strict';

angular.module('node', [])

.component('node', {

  templateUrl: 'partials/node.html',
  bindings: {
    data: '=node'
  },
  controller: ['$timeout', 'api', 'typeFactory', 'bsLoadingOverlayService', function($timeout, api, typeFactory, bsLoadingOverlayService) {

    var self = this;

    self.typeFactory = typeFactory;

    self.statuses = {
      SAVING: {
        label: "Saving",
        style: "alert-warning"
      },
      DELETING: {
        label: "Deleting",
        style: "alert-warning"
      },
      FAILED: {
        label: "Error",
        style: "alert-danger"
      },
      SUCCESS: {
        label: "Ok",
        style: "alert-success"
      }
    };
    self.status = null;

    self.duplicate = function() {
      var copy = angular.copy(self.data);
      copy.id = null;
      self.status = null;
      self.data = copy;
    };

    self.delete = function() {
      bsLoadingOverlayService.start();
      self.status = self.statuses.DELETING;
      api.deleteNode(self.data).then(
        function() {
          self.data = null;
          self.status = self.statuses.SUCCESS;
        },
        function() {
          if (self.status == self.statuses.DELETING) {
            self.status = self.statuses.FAILED;
          }
        },
        function(notify) {
          if (notify == "aborted")
            self.status = null;
        }
      )
      .finally(function() {
        bsLoadingOverlayService.stop();
      });
    };

    self.save = function() {
      bsLoadingOverlayService.start();
      self.status = self.statuses.SAVING;
      api.saveNode(self.data).then(
        function(node) {
          self.data = node;
          $timeout(function() {
            self.status = self.statuses.SUCCESS;
            bsLoadingOverlayService.stop();
          }, 200);
        },
        function() {
          self.status = self.statuses.FAILED;
          bsLoadingOverlayService.stop();
        }
      );
    };

  }]
})

.directive('nodeEdges', function() {

  return {
    templateUrl: 'partials/edges.html',
    restrict: 'E',
    scope: {
      node: '='
    },
    controller: ['$scope', '$timeout', 'api', 'events', 'bsLoadingOverlayService', function($scope, $timeout, api, events, bsLoadingOverlayService) {

      var self = $scope;

      self.nodes = api.nodes;

      self.$watch('node', function(node) {
        if (node && node.id) {
          self.edges = api.edges().filter(function(e) {
            return e.source == self.node.id || e.target == self.node.id;
          });
        }
      });

      $scope.$on(events.EDGE_ADDED, function(event, edge) {
        if (edge.source == self.node.id || edge.target == self.node.id)
          self.edges.push(edge);
      });

      $scope.$on(events.EDGE_DELETED, function(event, edge) {
        for (var idx in self.edges) {
          if (self.edges[idx].id == edge.id)
            self.edges.splice(idx, 1);
        }
      });

      $scope.deleteEdge = function(index) {
        bsLoadingOverlayService.start();
        api.deleteEdge(self.edges[index])
          .then(function() {
            $timeout(function() {
              bsLoadingOverlayService.stop();
            }, 400);
          });
      };

      $scope.saveEdge = function(index) {
        bsLoadingOverlayService.start();
        api.saveEdge(self.edges[index])
          .then(function() {
            $timeout(function() {
              bsLoadingOverlayService.stop();
            }, 400);
          });
      };

      $scope.contextKey = {};
      $scope.contextValue = {};
      $scope.addEdgeContext = function(index) {
        if (!$scope.edges[index].context)
          $scope.edges[index].context = {};
        $scope.edges[index].context[$scope.contextKey[index]] = $scope.contextValue[index];
        delete $scope.contextKey[index];
        delete $scope.contextValue[index];
      };

      $scope.removeFromContext = function(index, key) {
        delete $scope.edges[index].context[key];
      };
    }]
  };

});
