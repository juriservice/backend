/*global angular */
/*global console */

'use strict';

angular.module('api', [])

.constant('events', {

  NODE_ADDED: 10,
  NODE_UPDATED: 11,
  NODE_DELETED: 12,

  NODE_ADD: 15,
  NODE_SHOW: 16,
  NODE_DEL: 17,

  EDGE_ADDED: 20,
  EDGE_UPDATED: 21,
  EDGE_DELETED: 22,

  EDGE_ADD: 25,
  EDGE_DEL: 26,

  GRAPH_LOADED: 30,

})

.factory('actions', ['$rootScope', 'api', 'events', function($rootScope, api, events) {

  $rootScope.$on(events.EDGE_ADD, function(event, source, target) {
    var e = new api.Edge({
      source: source.id,
      target: target.id
    });
    api.saveEdge(e);
  });

  $rootScope.$on(events.EDGE_DEL, function(event, edge) {
    api.deleteEdge(edge);
  });

  $rootScope.$on(events.NODE_DEL, function(event, node) {
    if (node) {
      api.deleteNode(node);
    }
  });

  return {};

}])

.factory('api', ['$resource', '$q', '$rootScope', 'events', 'dialogs', function($resource, $q, $rootScope, events, dialogs) {
  var Node = $resource(
    '/admin/graph/node/:id',
    {id: '@id'},
    {update: {method: 'PUT'}}
  );
  var Edge = $resource(
    '/admin/graph/edge/:id',
    {id: '@id'},
    {update: {method: 'PUT'}}
  );

  angular.extend(Edge.prototype, {

    toString: function() {
      var src = getNode(this.source),
          dst = getNode(this.target);
      return src.title + " (" + src.id.substr(0, 6) +") → " +
             dst.title + " (" + dst.id.substr(0, 6) +")";
    }

  });

  var nodes = [],
      edges = [];

  function getNode(id) {
    return nodes.filter(function(n) {
      if (n.id == id)
        return true;
    })[0] || null;
  }

  function reload() {
    var tmpNodes = Node.query(),
        tmpEdges = Edge.query();
    return $q.all([tmpNodes.$promise, tmpEdges.$promise])
      .then(
        function() {
          nodes = tmpNodes;
          edges = tmpEdges;
          $rootScope.$broadcast(events.GRAPH_LOADED);
        },
        function(error) {
          console.log("failed to load graph");
        }
      );
  }
  reload();

  return {
    Node: Node,
    Edge: Edge,
    nodes: function() {
      return nodes;
    },
    edges: function() {
      return edges;
    },
    reload: reload,
    getNode: getNode,
    fetchNode: function(node) {
      return node.$get();
    },
    saveNode: function(node) {
      if (node.id) {
        return node.$update().then(
          function(node) {
            $rootScope.$broadcast(events.NODE_UPDATED, node.id);
            return node;
          }
        );
      } else {
        return node.$save().then(
          function(node) {
            nodes.push(node);
            $rootScope.$broadcast(events.NODE_ADDED, node.id);
            return node;
          }
        );
      }
    },
    deleteNode: function(node) {
      return dialogs.confirm('Node will be deleted')
        .then(angular.bind(this, function() {
          // TODO: to this in the backend?
          var edgesToDelete = edges.filter(function(e) {
            return e.source == node.id || e.target == node.id;
          });
          edgesToDelete.forEach(angular.bind(this, function(e) {
            this.deleteEdge(e, true);
          }));
          return node.$delete().then(function(node) {
            nodes.filter(function(n) {
              return n.id != node.id;
            });
            $rootScope.$broadcast(events.NODE_DELETED, node.id);
          });
        }));
    },
    saveEdge: function(edge, force) {
      if (edge.id) {
        return edge.$update().then(
          function(edge) {
            $rootScope.$broadcast(events.EDGE_UPDATED, edge);
            return edge;
          }
        );
      } else {
        return dialogs.confirm("Add link", edge.toString(), force)
          .then(function() {
            return edge.$save()
              .then(function(edge) {
                console.log(edge);
                edges.push(edge);
                $rootScope.$broadcast(events.EDGE_ADDED, edge);
                return edge;
              });
          });
      }
    },
    deleteEdge: function(edge, force) {
      return dialogs.confirm("Remove link", edge.toString(), force)
        .then(function() {
          return edge.$delete().then(
            function(edge) {
              $rootScope.$broadcast(events.EDGE_DELETED, edge);
              return edge;
            }
          );
        });
    }
  };

}]);
