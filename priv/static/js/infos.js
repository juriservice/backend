/*global angular */
/*global console */

'use strict';

angular.module('infos', [])

.directive('infosForm', function() {

  return {
    templateUrl: 'partials/infos.html',
    restrict: 'E',
    scope: {
      infos: '='
    },
    controller: ['$scope', 'infoTypes', function($scope, infoTypes) {
      $scope.removeInfo = function(index) {
        $scope.infos.splice(index, 1);
      };

      $scope.addInfo = function(type) {
        $scope.infos.push(angular.copy(infoTypes[type].skel));
      };

      $scope.infoTypes = infoTypes;
    }]
  };

})

.directive('infoForm', function() {

  return {
    templateUrl: 'partials/info.html',
    restrict: 'E',
    scope: {
      info: '=',
      index: '='
    },
    link: function postLink(scope) {
      scope.template = 'partials/infos/'+scope.info.type+'.html';
    },
    controller: ['$scope', 'infoTypes', 'typeFactory', function($scope, infoTypes, typeFactory) {
      $scope.infoTypes = infoTypes;

      $scope.removeInfo = function(index) {
        $scope.$parent.removeInfo(index);
      };

      $scope.typeFactory = typeFactory;
    }]
  };

})

.controller('infosCtrl', ['$scope', 'infoTypes', function($scope, infoTypes) {
  this.types = infoTypes;
}])

.controller('radioChoiceCtrl', ['$scope', function($scope) {
  this.addChoice = function() {
    $scope.info.choices.push({
      key: null,
      name: null,
      desc: null
    });
  };
  this.removeChoice = function(index) {
    $scope.info.choices.splice(index, 1);
  };
}])

.controller('inputInfoCtrl', ['$scope', function($scope) {
  this.toggleApi = function() {
    if ($scope.info.api) {
      $scope.info.api = null;
    }
    else {
      $scope.info.api = {
        base_url: null,
        params: null,
        path: null
      };
    }
  };
}]);
