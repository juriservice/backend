/*global angular */
/*global console */

'use strict';

angular.module('tree', [])

.factory('tree', ['$rootScope', 'api', 'events', 'dialogs', function($rootScope, api, events, dialogs) {

  var edges = {},
      nodes = {},
      root = [];

  function Node(node) {
    this.node = node;
    this._active = false;
    this.open = false;
  }

  Node.prototype = {

    get title() {
      return this.node.title;
    },

    get id() {
      return this.node.id;
    },

    get order() {
      return this.node.order;
    },

    get children() {
      var children = [];
      for (var edgeId in edges) {
        if (edges[edgeId].source == this.node.id)
          children.push(nodes[edges[edgeId].target]);
      }
      return children.sort(function(a, b) {
        return a.node.order - b.node.order;
      });
    },

    get parents() {
      var parents = [];
      for (var edgeId in edges) {
        if (edges[edgeId].target == this.id)
          parents.push(nodes[edges[edgeId].source]);
      }
      return parents;
    },

    get parentEdges() {
      var pEdges = [];
      for (var edgeId in edges) {
        if (edges[edgeId].target == this.id)
          pEdges.push(edges[edgeId]);
      }
      return pEdges;
    },

    get childEdges() {
      var cEdges = [];
      for (var edgeId in edges) {
        if (edges[edgeId].source == this.id)
          cEdges.push(edges[edgeId]);
      }
      return cEdges;
    },

    get active() {
      return this._active;
    },

    setActive: function() {
      for (var nodeId in nodes) {
        nodes[nodeId]._active = false;
      }
      this._active = true;
    },

    toggle: function(recursive, open) {
      if (typeof open == "undefined")
        open = !this.open;
      this.open = open;
      if (typeof recursive != "undefined") {
        this.children.forEach(function(child) {
          child.toggle(recursive, open);
        });
      }
    },

    edgeContext: function(parentNode) {
      var mdata = this.parentEdges.reduce(function(mdata, e) {
        if (e.source == parentNode.id && e.context) {
          for (var k in e.context) {
            var value = e.context[k];
            if (value.startsWith("#")) {
              mdata.push(value.substr(1).trim());
            } else {
              mdata.push(k + " == \"" + e.context[k] + "\"");
            }
          }
        }
        return mdata;
      }, []);

      if (mdata.length > 0) {
        return mdata.join(', ');
      }
      return '';
    }
  };

  function initTree() {
    api.edges().forEach(function(e) {
      edges[e.id] = e;
    });
    api.nodes().forEach(function(n) {
      nodes[n.id] = new Node(n);
    });
    root.splice(0, root.length);
    // get root nodes list
    for (var nodeId in nodes) {
      if (nodes[nodeId].parents.length === 0)
        root.push(nodes[nodeId]);
    }
  }

  function activeNode() {
    for (var idx in nodes) {
      if (nodes[idx].active === true)
        return nodes[idx];
    }
    return null;
  }

  $rootScope.$on(events.GRAPH_LOADED, function() {
    initTree();
  });

  $rootScope.$on(events.NODE_ADDED, function(event, id) {
    var node = new Node(api.getNode(id));
    node.setActive();
    nodes[node.id] = node;
    if (node.parents.length === 0)
      root.push(node);
  });

  $rootScope.$on(events.NODE_DELETED, function(event, id) {
    delete nodes[id];
    for (var idx in root) {
      if (root[idx].id == id) {
        root.splice(idx, 1);
      }
    }
  });

  $rootScope.$on(events.EDGE_ADDED, function(event, edge) {
    edges[edge.id] = edge;
    for (var idx in root) {
      if (root[idx].parents.length > 0) {
        root.splice(idx, 1);
      }
    }
  });

  $rootScope.$on(events.EDGE_DELETED, function(event, edge) {
    delete edges[edge.id];
    for (var nodeId in nodes) {
      if (nodes[nodeId].parents.length === 0)
        if (root.indexOf(nodes[nodeId]) === -1)
          root.push(nodes[nodeId]);
    }
  });

  return {
    rootNodes: root,

    addEdge: function(tNode) {
      var source = activeNode();
      if (!source) {
        dialogs.error('No active parent node to link to');
        return;
      }
      if (source.id == tNode.id) {
        dialogs.error("Can't link node to itself");
        return;
      }
      var edges = tNode.parentEdges.filter(function(e) {
        return e.source == source.id;
      });
      if (edges.length > 0) {
        dialogs.error("Link already present");
        return;
      }
      $rootScope.$emit(events.EDGE_ADD, source, tNode.node);
    },

    deleteEdge: function(tNode, parentNode) {
      console.log(parentNode);
      if (!parentNode) {
        dialogs.error('Node has no parent');
        return;
      }
      var edges = tNode.parentEdges.filter(function(e) {
        return e.source == parentNode.id;
      });
      if (edges.length === 0) {
        dialogs.error("Link not found");
        return;
      }
      $rootScope.$emit(events.EDGE_DEL, edges[0]);
    },

    showNode: function(tNode) {
      tNode.setActive();
      $rootScope.$broadcast(events.NODE_SHOW, tNode.node);
    },

    deleteNode: function(tNode) {
      $rootScope.$emit(events.NODE_DEL, tNode.node);
    },

    addChildNode: function(tNode) {
      var node = new api.Node({
        title: "New node",
        type: "info",
        infos: []
      });
      api.saveNode(node)
        .then(function(n) {
          var edge = new api.Edge({
            source: tNode.id,
            target: n.id
          });
          api.saveEdge(edge, true).then(function() {
            tNode.toggle(false, true);
            $rootScope.$broadcast(events.NODE_SHOW, node);
          });
        });
    },
  };

}])

.directive('tree', ['RecursionHelper', 'events', function(RecursionHelper, events) {

  return {
    templateUrl: 'partials/tree.html',
    restrict: 'E',
    scope: {
      tNode: '<node',
      parent: '<parent',
    },
    compile: function(element) {
      // Use the compile function from the RecursionHelper,
      // And return the linking function(s) which it returns
      return RecursionHelper.compile(element);
    },
    controller: function($scope, tree) {
      $scope.tree = tree;

      $scope.toggleNode = function(tNode, event) {
        if (event.shiftKey)
          tNode.toggle(true);
        else
          tNode.toggle();
      };
    }
  };

}]);
