# Juriservice

## Dev install

  1. Install elixir: http://elixir-lang.org/install.html

  2. `git clone git@framagit.org:juriservice/backend.git`

  3. `cd backend && mix deps.get`

  4. `git submodule init`

  5. `git submodule update`

  6. `iex -S mix`

  7. You can go to `http://localhost:4001`

## Style guide

https://github.com/levionessa/elixir_style_guide

## API entrypoints

    Graph: http://localhost:4001/graph/root/next

    Admin: http://localhost:4002/admin/index.html
